package singleton;

import java.sql.SQLException;

class Application {
    public static void main(String[] args) throws SQLException{
        Database foo = Database.getInstance();
        foo.query("select * from table1");
        Database bar = Database.getInstance();
        bar.query("SELECT * from table2");
    
    }
}


class Database {
    private static Database instance = new Database();
    private Connection connection;
    
    private Database() {
    	if(this.connection == null) {
            try {
				this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dbcrm", "postgres", "789");
			} catch (SQLException e) {
				e.printStackTrace();
			  }
    	}
    }
    public static synchronized Database getInstance() {
            if (Database.instance == null) {
            	Database.instance = new Database();
            }
            return Database.instance;
    }
    public void query(String q) throws SQLException{
            PreparedStatement query = connection.prepareStatement(q);
                ResultSet data = query.executeQuery(); 
            while (data.next()) {
                System.out.println("id: " + data.getInt(1));
                System.out.println("name: " + data.getString(2));
            }
    }
}
