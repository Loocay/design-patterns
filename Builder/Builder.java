public class Builder {
    public Builder(){}

    public void buildWalls(){
        System.out.println("House wall built!");
    }

    public void buildDoor(){
        System.out.println("House door built");
    }

    public void buildWindows(){
        System.out.println("House windows built!");
    }

    public void buildRoof(){
        System.out.println("House roof built!");
    }

    public void getResult(){
        System.out.println("House ready!");
    }
}